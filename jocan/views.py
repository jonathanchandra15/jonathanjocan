from django.shortcuts import render

def home(request):
    return render(request, 'jonathanjocan.html')

def portofolio(request):
    return render(request, 'portofolio.html')